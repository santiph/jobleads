# Frontend Task 5 - SPA Application


For a given json structure:
```
[
  {
    event: "First Call with Jobleads",
    date: "2018-10-20",
    time: "08:00",
    location: "Anywhere",
    participants: ["John Doe", "Max Musterman"]
  },
  {
    event: "Training Day",
    date: "2018-10-29",
    time: "08:00",
    location: "Hamburg",
    participants: ["John Doe", "Max Musterman"]
  },
  {
    event: "First day at work",
    date: "2018-12-01",
    time: "08:00",
    location: "Hamburg",
    participants: ["John Doe", "Max Musterman", "Cris Sullivan", "Mark O'Polo"]
  }
]
```

## Build a SPA application fulfilling the following tasks:

1. Fetch from api and display the list of events (it should be a component reading the data from API).
2. Store initial api result in local storage and from now on - synchronize every change
2. Possibility to remove the element from the list (it should be saved to local data storage on every change)
3. Possibility to edit the element properties (keep it simple)
4. Possibility to add new element to the list
5. Possibility to clear all elements of the list
6. Possibility to clear data storage

Keep in mind that API is read only so:

1. Use it only for the first time fetch
2. perform further Create / Read /  Edit / Delete operations locally synchronizing with local storage

Hints:

You can use one of JavaScript frameworks - VueJs / Angular / React (if you know other then use it too, or you think it can be achieved with plain JavaScript)
Split application logic into components (ex. events list, event form)
Keep it really simple because of limited time.



How to run the API server

Form the command line go to the assessment folder and then to Task 5 folder, then run this command

```
php -S 127.0.0.1:12345 server.php
```

You should have an output with server logs

So now you can access api via http://127.0.0.1:12345/api

Make the solution index.html and assets next to server.php so you can access them from the server url http://127.0.0.1:12345/.
