import React, { Component } from 'react';
import './App.css';

import Layout from './Components/Layout';
import Header from './Components/Header';
import NewEvent from './Components/NewEvent';
import EventsList from './Components/EventsList/Table';

import Services from './Services';

class App extends Component {

  state = {
    events: [],
    displayNewEventForm: false,
  }

  async componentDidMount() {
    const events = await Services.getEvents();
    this.setState({ events });
  }

  onAddNewEvent = (newEvent) => {
    // 1. Send it to Services
    const newCollection = Services.addEvent(newEvent);
    // 2. On success, update our state & hide form
    this.setState({
      events: newCollection,
      displayNewEventForm: false,
    });
  }

  onShowNewEventForm = () => {
    this.setState({ displayNewEventForm: true });
  }

  onEditEvent = (updatedEvent) => {
    const events = Services.editEvent(updatedEvent);

    // Persist changes
    this.setState({
      events,
    });
  }

  onRemoveEvent = (id) => {
    //  Find event & filter it out
    const newEventList = Services.removeEvent(id);
    // Persist changes
    this.setState({
      events: newEventList,
    });
  }

  onClearLocalStorage = () => {
    Services.clearLocalStorage();
    this.setState({ events: [] });
  }

  onClearList = () => {
    Services.clearList();
    this.setState({ events: [] });
  }

  render() {
    const { events, displayNewEventForm } = this.state;
    return (
      <Layout>
        <Header onClearLocalStorage={this.onClearLocalStorage} onClearList={this.onClearList} />
        <EventsList
          events={events}
          onEditEvent={this.onEditEvent}
          onRemoveEvent={this.onRemoveEvent}
        />
        { displayNewEventForm
          ? (<NewEvent onAddNewEvent={this.onAddNewEvent} />)
          : (<button type="button" className="btn btn-light" onClick={this.onShowNewEventForm}>Add new</button>)
        }
        
      </Layout>
    );
  }
}

export default App;
