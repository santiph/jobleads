import React, { Component } from 'react';

class NewEvent extends Component {
  state = {
    name: '',
    location: '',
    date: '',
    time: '',
    participants: '',
  };

  handleChange = event => {
    this.setState({
      [event.currentTarget.name]: event.currentTarget.value,
    });
  }

  onFormSubmit = event => {
    const { onAddNewEvent } = this.props;
    // (explicitly picking attributes to prevent new props from being added by accident)
    const {
      name,
      location,
      date,
      time,
      participants,
    } = this.state;
    // Prevent form submit
    event.preventDefault();

    // Build an new Event model 
    const newEvent = {
      event: name,
      location,
      date,
      time,
      participants: participants.split(',')
        .map(participant => participant.trim()),
    };

    // Process it
    onAddNewEvent(newEvent);
  }

  render() {
    const {
      name,
      location,
      date,
      time,
      participants,
    } = this.state;

    return (
      <section className="col-6">
        <h1>New Event Form</h1>
        <form onSubmit={this.onFormSubmit}>
          <div className="form-group row">
            <label htmlFor="staticEmail" className="col-sm-4 col-form-label">Event name</label>
            <div className="col-sm-8">
              <input
                type="text"
                name="name"
                className="form-control"
                value={name}
                onChange={this.handleChange}
                placeholder="Event name" />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="staticEmail" className="col-sm-4 col-form-label">Event location</label>
            <div className="col-sm-8">
              <input
                type="text"
                name="location"
                className="form-control"
                value={location}
                onChange={this.handleChange}
                placeholder="Event location" />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="staticEmail" className="col-sm-4 col-form-label">Date</label>
            <div className="col-sm-8">
              <input
                type="date"
                name="date"
                className="form-control"
                value={date}
                onChange={this.handleChange}
                placeholder="Date" />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="staticEmail" className="col-sm-4 col-form-label">Time</label>
            <div className="col-sm-8">
              <input
                type="time"
                name="time"
                className="form-control"
                value={time}
                onChange={this.handleChange}
                placeholder="Time" />
            </div>
          </div>
          <div className="form-group row">
            <label htmlFor="staticEmail" className="col-sm-4 col-form-label">Participants</label>
            <div className="col-sm-8">
              <input
                type="text"
                name="participants"
                className="form-control"
                value={participants}
                onChange={this.handleChange}
                placeholder="Participants"
              />
              <span>Comma separated names</span>
            </div>
          </div>
          <button type="submit" className="btn btn-primary mb-2">Create new event</button>
        </form>
      </section>
    );
  };
}

export default NewEvent;
