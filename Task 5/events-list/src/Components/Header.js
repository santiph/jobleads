import React from 'react';

const Header = ({ onClearLocalStorage, onClearList }) => {
  return (
    <div className="row">
      <div className="col">
        <h1>Events list</h1>
      </div>
      <div className="col text-right">
        <button onClick={onClearLocalStorage} type="button" className="btn btn-danger mx-2">
          Clear local storage
        </button>
        <button onClick={onClearList} type="button" className="btn btn-danger">
          Clear list
        </button>
      </div>
    </div>
  );
};

export default Header;