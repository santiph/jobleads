import React from 'react';

const Layout = ({ children }) => {
  return (
    <div className="container my-2">
      <div className="row">
        <div className="col">
          { children }
        </div>
      </div>
    </div>
  );
};

export default Layout;