import React, { Component } from 'react';

class Row extends Component {
  constructor(props) {
    super(props);

    const {
      eventDetails: {
        event,
        date,
        time,
        location,
        participants,
      },
    } = props;

    this.state = {
      editMode: false,

      name: event,
      date,
      time,
      location,
      participants,
    }
  }

  handleChange = event => {
    this.setState({
      [event.currentTarget.name]: event.currentTarget.value,
    });
  }

  onFormSubmit = event => {
    const { eventDetails: { id }, onEditEvent } = this.props;
    // (explicitly picking attributes to prevent new props from being added by accident)
    const {
      name,
      location,
      date,
      time,
      participants,
    } = this.state;
    // Prevent form submit
    event.preventDefault();

    // Build an new Event model 
    const newEvent = {
      id,
      event: name,
      location,
      date,
      time,
      participants: [participants],
    };

    // Process it
    onEditEvent(newEvent);

    // Disable Edit More
    this.setState({ editMode: false });
  }

  onShowEditForm = () => {
    this.setState({ editMode: true });
  }

  getEventInfo() {
    const {eventDetails, onRemoveEvent} = this.props;
    const {
      id,
      event,
      date,
      time,
      location,
      participants,
    } = eventDetails;

    return (
      <tr>
        <th scope="row">{event}</th>
        <td>{date} {time}</td>
        <td>{location}</td>
        <td>{participants.join(', ')}</td>
        <td>
          <button type="button" className="btn btn-info mx-2" onClick={this.onShowEditForm}>Edit</button>
          <button type="button" className="btn btn-danger" onClick={() => onRemoveEvent(id)}>Remove</button>
        </td>
      </tr>
    );
  }

  getEditEvent = () => {
    const {eventDetails: { id }, onRemoveEvent} = this.props;
    const {
      name,
      location,
      date,
      time,
      participants,
    } = this.state;

    return (
      <tr>
        <th scope="row">
          <input
            type="text"
            name="name"
            className="form-control"
            value={name}
            onChange={this.handleChange}
            placeholder="Event name"
            />
        </th>
        <td>
          <div className="row">
            <div className="col">
              <input
                type="date"
                name="date"
                className="form-control"
                value={date}
                onChange={this.handleChange}
                placeholder="Event Date"
              />
            </div>
            <div className="col">
              <input
                type="time"
                name="time"
                className="form-control"
                value={time}
                onChange={this.handleChange}
                placeholder="Event Time"
                />
            </div>
          </div>
        </td>
        <td>
          <input
            type="text"
            name="location"
            className="form-control"
            value={location}
            onChange={this.handleChange}
            placeholder="Event Location"
            />
          </td>
        <td>{participants.join(', ')}</td>
        <td>
          <button type="button" className="btn btn-info mx-2" onClick={this.onFormSubmit}>Save</button>
          <button type="button" className="btn btn-danger" onClick={() => onRemoveEvent(id)}>Remove</button>
        </td>
      </tr>
    );
  }

  render() {
    const { editMode } = this.state;

    return editMode
      ? this.getEditEvent()
      : this.getEventInfo();
  }
}

export default Row;
