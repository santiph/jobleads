import React from 'react';
import Row from './Row';

const EventsList = ({ events, onEditEvent, onRemoveEvent }) => {

  return (
    <table className="table">
      <thead>
        <tr>
          <th scope="col">Name</th>
          <th scope="col">When</th>
          <th scope="col">Where</th>
          <th scope="col">Participants</th>
          <th scope="col"></th>
        </tr>
      </thead>
      <tbody>
        {(!Array.isArray(events) || events.length === 0) && <tr><td>No events Available</td></tr>}
        {Array.isArray(events) && events.length > 0 && events.map((event) => (
          <Row
            key={event.id}
            eventDetails={event}
            onEditEvent={onEditEvent}
            onRemoveEvent={onRemoveEvent}
          />
        ))}
      </tbody>
    </table>
  );
};

export default EventsList;