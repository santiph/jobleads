import axios from 'axios';

const SERVER_URL = 'http://localhost:12345';
const eventsCollection = [];
let lastIndex = 0;

const saveToLocalStorage = (collection) => {
  window.localStorage.setItem('eventsCollection', JSON.stringify(collection));
}

const readFromLocalStorage = async () => {
  const storedCollection = window.localStorage.getItem('eventsCollection');
  const parsedEvents = JSON.parse(storedCollection);
  if (Array.isArray(parsedEvents)) {
    // If the events collection is available, return it
    return parsedEvents;
  } else {
    // Otherwise, fetch it from services
    const results = await axios.get(`${SERVER_URL}/api`);
    if (Array.isArray(results.data)) {
      // persist fetched results to Local Storage
      saveToLocalStorage(results.data);
      return results.data;
    } else {
      // if request failed, initialize empty storage
      saveToLocalStorage([]);
      console.error('Error on request', results);
      return [];
    }
  }
}

export const clearLocalStorage = () => {
  window.localStorage.clear();
}

export const clearList = () => {
  eventsCollection.length = 0;
}

export const getEvents = async () => {
  if (Array.isArray(eventsCollection) && eventsCollection.length > 0) {
    return eventsCollection;
  } else {
    const currentStorage = await readFromLocalStorage();
    eventsCollection.push(...currentStorage);

    // Update lastIndex
    const indexesCollection = eventsCollection.map(event => event.id);
    lastIndex = Math.max(...indexesCollection);
    return eventsCollection;
  }
}
  
export const addEvent = newEvent => {
  // Add new Event to the persisted collection
  eventsCollection.push({
    id: lastIndex + 1,
    ...newEvent,
  });

  // Update Localstorage
  saveToLocalStorage(eventsCollection);

  // Update lastIndex
  lastIndex++;

  // Return the new collection 
  return eventsCollection;
}

export const removeEvent = eventId => {
  // find event index in the current collection
  const eventIndex = eventsCollection.findIndex(event => event.id === eventId);

  // Remove it from the current collection
  eventsCollection.splice(eventIndex, 1);

  // Update Localstorage
  saveToLocalStorage(eventsCollection);

  // Return resulting collection
  return eventsCollection;
}

export const editEvent = updatedEvent => {
  // 1. Grab existing event
  const eventIndex = eventsCollection.findIndex(event => event.id === updatedEvent.id);

  // 2. Update it in the current collection
  eventsCollection[eventIndex] = updatedEvent;

  // Update Localstorage
  saveToLocalStorage(eventsCollection);

  // Return resulting collection
  return eventsCollection;
}

export default {
  addEvent,
  clearLocalStorage,
  clearList,
  editEvent,
  getEvents,
  removeEvent,
};