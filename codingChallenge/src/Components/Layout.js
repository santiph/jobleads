import React from 'react';
import logo from './images/logo_JL.svg';

const Layout = ({ children }) => {
  return (
    <>
      <div className="row no-gutters">
        <div className="col-sm">
          <div className="logo-container w-100">
            <img src={logo} className="logo mx-auto d-block" alt="logo" />
          </div>
        </div>
      </div>
      { children }
    </>
  );
};

export default Layout;