import React, { Component } from 'react';

// Font Awesome
import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCheck, faCheckCircle, faTimes } from '@fortawesome/free-solid-svg-icons'

// Styles
import './App.css';

// Images
import logo from './images/logo_JL.svg';
import moodMobile from './images/mood-mobile.jpg';

// Add FontAwesome to the library
library.add(faCheck, faCheckCircle, faTimes);

class App extends Component {
  render() {
    return (
      <>
        <div className="row no-gutters d-lg-none">
          <div className="col-sm">
            <div className="d-inline-block bg-white w-100 p-2">
              <img src={logo} className="logo mx-auto d-block" alt="logo" />
            </div>
          </div>
        </div>
        <div className="container h-100">
          <div className="row" id="mainContainer">
            <div className="col-sm-12 mx-n1 col-lg-5">

              <div className="d-none d-lg-inline-block bg-white p-2 my-2">
                <img src={logo} className="logo mx-auto d-block" alt="logo" />
              </div>
              {/* Card Start */}
              <div id= "offer" className="card bg-transparent border-0">
                <div className="card-header">
                  <div className="product">
                    <p className="lead font-weight-light text-justify text-white">Sehr geehrter Herr Mustermann, starten Sie sauber ins jahr 2015 mit neuen Perspektiven.</p>
                    <img
                      alt="Mood"
                      className="mx-auto d-block d-lg-none"
                      src={moodMobile}
                    />
                  </div>
                </div>
                <div className="card-body bg-light mx-n3 mx-lg-0">
                  <ul className="benefits list-unstyled">
                    <li>
                      <FontAwesomeIcon
                        className="mr-2"
                        icon="check"
                      />
                      <span className="card-text">Unbegrenzte Ansicht aller Premium-Jobs</span>
                    </li>
                    <li>
                      <FontAwesomeIcon
                        className="mr-2"
                        icon="check"
                      />
                      <span className="card-text">Geyielte Filtermöglichkeiten nach Jobs mit mehr Gehalt</span>
                    </li>
                  </ul>
                  <div className="container h-100">
                    <div className="row">
                      <div className="col-xs-auto">
                        <FontAwesomeIcon
                          color="#c1e639"
                          icon="check-circle"
                          size="4x"
                        />
                      </div>
                      <div className="col">
                        <span className="d-block h3 font-weight-bold">EUR 79,40</span>
                        <span className="d-block"><del>EUR 13,23 pro Monat</del></span>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="card-footer bg-light mx-n3 p-0 mx-lg-0">
                  <button
                    aria-controls="moreInfo"
                    aria-expanded="false"
                    className="btn btn-primary btn-lg btn-block rounded-0 collapsed grow"
                    data-target="#moreInfo"
                    data-toggle="collapse"
                    id="infoButton"
                    type="button"
                  >
                    Jetzt säubern und neuen Job Finden
                  </button>
                </div>
              </div>
              {/* Card Ends */}

            </div>

            {/*
              * Since mx-n1 interferes with offset-* classes
              * Push Collapse component 4 columns to the right on LG screens,
              * while leaving right below on XS and SM
              * Horrible hack, but prevents javascript
              **/}
            <div className="col-lg-3"></div>

            <div className="col-sm-12 mx-n1 col-lg-4 offset-lg-4">
              {/* More Info Starts */}
              <div className="collapse mx-n3" id="moreInfo">
                <div className="card rounded-0">
                  <div className="card-header">
                    <button
                      aria-controls="moreInfo"
                      aria-expanded="false"
                      aria-label="Close"
                      className="close"
                      data-target="#moreInfo"
                      data-toggle="collapse"
                      type="button"
                    >
                      <span aria-hidden="true">
                        <FontAwesomeIcon
                          icon="times"
                        />
                      </span>
                    </button>
                    <h5 className="modal-title">Mehr Informationen:</h5>
                  </div>
                  <div className="card-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam elementum metus lectus. Quisque non feugiat ex. Fusce pellentesque augue et lacus vulputate, at auctor sapien feugiat. Suspendisse ex nibh, vehicula vitae lacus non, convallis iaculis libero. Suspendisse potenti. Suspendisse nibh lacus, elementum ut metus in, egestas posuere tellus. Ut egestas consequat elit, quis tempor tortor placerat eget.</p>
                  </div>
                </div>
              </div>
              {/* More Info Ends */}

            </div>
          </div>

          <div className="row mt-3">
            <div className="col-sm mx-n1">
              {/* Footer */}
              <p className="text-secondary text-center">&copy; 2014 JobLeads GmbH - Alle Rechte vorbehalten</p>
            </div>
          </div>
        </div>
      </>
    );
  }
}

export default App;
